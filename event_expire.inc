<?php
/**
 * @file
 * Include file for the event_expire module.
 * Contains page callbacks.
 */

/**
 * Display test page
 *
 * @param daysafter
 *   Test with this value for the daysafter variable
 *   If it is not set, then the value set on the admin page is used.
 *
 * @return
 *   test page listing the events that would be expired had this value been set and the cron enabled
 */
function event_expire_test($daysafter = -1) {
  $nodetype = variable_get('event_expire_type', '');
  $contenttype = variable_get('event_expire_content', '');
  $datefield = variable_get('event_expire_field', '');
  if ($daysafter == -1 ) {
    $daysafter = variable_get('event_expire_days', 60);
  }

  if (($contenttype == '') || ($nodetype == '') || ($datefield == '')) {
    return "Please set up admin values and return to this page";
  }

  //calculate the unix timestamp for expiry
  //$daysafter = 44;
  $expiretime = time() - 60 * 60 * 24 * $daysafter;
  $sql = "SELECT n.nid, n.title FROM {node} n
            JOIN {content_type_%s} AS c ON c.nid = n.nid
            WHERE n.type = '%s' AND status = 1
              AND UNIX_TIMESTAMP(c.field_%s) < %d";
  $result = db_query($sql, $contenttype, $nodetype, $datefield, $expiretime);

  $page = '<p>' . t('List of nodes of type: [@type] in content: [@content] and field: [content_type_@field] that are more than @days days old',
                    array('@type' => $nodetype, '@content' => $contenttype, '@field' => $datefield, '@days' => $daysafter)) . '</p>';
  $page .= '<p>' . t('Test with a different number of days by appending the number to the URL of this page.') . '</p>';
  $page .= '<p>' . t('Module running on cron: ') . (variable_get('event_expire_enabled', 0) ? 'ENABLED' : 'DISABLED') . '</p><ul>';

  //loop through the events found, adding them to the page
  while ($node = db_fetch_object($result)) {
    $page .= '<li>' . l($node->title, 'node/' . $node->nid) . '</li>';
  }
  return $page . '</ul>';
}

/**
 * Display test page
 *
 * @param oldnode
 *   The nid of the unpublished node
 *
 * @return
 *   Page that informs the user of the name and date of the expired event
 */
function event_expire_redirect($oldnodeid) {
  $nodetype = variable_get('event_expire_type', '');
  $contenttype = variable_get('event_expire_content', '');
  $datefield = variable_get('event_expire_field', '');
  $sql = "SELECT n.title, c.field_%s AS eventdate, UNIX_TIMESTAMP(c.field_%s) AS eventstamp FROM {node} n
            JOIN {content_type_%s} AS c ON c.nid = n.nid
            WHERE n.nid = %d";
  $result = db_query($sql, $datefield, $datefield, $contenttype, $oldnodeid);
  if ( $data = db_fetch_object($result) ) {
    $page = '<h4>' . t('The Event:') . '</h4>';
    $page .= '<h2>' . $data->title . '</h2>';
    $page .= '<h4>' . t('took place on:') . '</h4>';
    $date = date_make_date($data->eventdate, 'UTC', DATE_ISO); // shift date to default timezone
    date_timezone_set($date, timezone_open(date_default_timezone_name(FALSE)));
    $page .= '<h2>' . date_format_date($date, 'large') . '</h2>';
    $page .= '<p>' . t('Other information about this event is no longer available.') . '</p>';
  }
  else {
    $page = '<h2>' . t('Event not found for node') . ': ' . $oldnodeid . '</h2>';
  }
  return $page;
}

