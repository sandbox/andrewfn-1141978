
ABOUT THIS MODULE
=================

Back in 2006 (Drupal 4.7) I wrote a module for the automatic unpublishing of
events after the date had passed. Rather than create a contrib module, I simply
pasted in a forum: node/77166. Various people suggested improvements and over
time it has been upgraded to D5 and then D6. The Events module has given way to
the CCK date field and the module has picked up some flexibility along the way.
Finally it has been re-named event_expire and I have decided it was time to
contribute it properly.

 * The module is currently in use on the Toronto TD Jazz Festival for
   unpublishing concerts 60 days after events.
 * In my experience it solves such a common problem it is well worth making it
   available in the contrib repository.

DESCRIPTION
===========

 * Events that use the CCK date field are automatically unpublished an
   arbitrary number of days after the date.
 * The module is *extremely simple to configure*--simply enter the name of the
   content type and field, and the number of days before expiry.
 * This is only done once, not on each and every node.
 * A test mode is provided to show which nodes would be unpublished.

DIFFERENCES FROM OTHER SIMILAR MODULES
======================================

 * Scheduler
    o A very powerful module that provides the ability to schedule events on
      nodes.
    o This is unsuitable for the present use-case because it requires
      duplication of the work in entering dates, with the associated
      opportunity for error in constantly having to calculate the expiry date.
 * Node Expire
    o Provides a count-down timer on each node.
    o Similar problem to that of scheduler. It is not automated and there is
      extra work to be done each time a node is created.
 * Auto Expire
    o The project is not actively maintained
    o The use-case for this project is a site with multi-authored content where
      content will by default be removed a certain number of days after it is
      created.
    o A sophisticated system of email messages informs the author of the
      imminent removal of content and gives the option of extending it.
    o As with the two previous modules, there is extra expiry data attached to
      each node.
    o There is no means for using the CCK date field to control expiry, and to
      modify the project to do this would take it away from its natural use-
      case
 * Rules
    o The closest solution for this problem is to configure rules to accomplish
      what is needed
    o A tutorial is available for doing this, but there are several major
      disadvantages
    o Setup is complex and fraught with pitfalls, as can be seen in this
      discussion
    o It is very messy to apply retroactively to a site, i.e. to old content
    o Once rules have been set up on each node, they are very messy to change.
      For example if the number of days between an event its expiry were to
      change, then all the rules would have to be removed and re-written.

CONFIGURATION
=============

1. Set the values in the administration page (yoursite/admin/settings/
   event_expire), but leave cron disabled.
2. Visit the test page (yoursite/event_expire/test/) to make sure the nodes you
   expect to be unpublished are listed.
3. Return to the administration page and enable the module to run on cron, and
   check recent log entries to see it working.

POSSIBLE FUTURE DEVELOPMENT
===========================

 * Set expiry date in metatag to warn Google

SUPPORT
=======

If you experience a problem with Event Expire or have a problem, file a request
or issue on the Event Expire queue at http://drupal.org/project/issues/1106760.
DO NOT POST IN THE FORUMS. Posting in the issue queues is a direct line of
communication with the module authors.

No guarantee is provided with this software, no matter how critical your
information, module authors are not responsible for damage caused by this
software or obligated in any way to correct problems you may experience.


SPONSORS
========

The Event Expire module is sponsored by writup.org
( http://writeup.org ).

Licensed under the GPL 2.0.
http://www.gnu.org/licenses/gpl-2.0.txt

